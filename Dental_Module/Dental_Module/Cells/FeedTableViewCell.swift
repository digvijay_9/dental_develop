//
//  FeedTableViewCell.swift
//  Dental_Module
//
//  Created by digvijay mallegowda on 2/10/19.
//  Copyright © 2019 digvijay mallegowda. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {
    
    var profileContainerView : UIView!
    var profileImageView : UIView!
    var profileImage : UIImageView!
    var profileName : UILabel!
    var profileDesignation : UILabel!
    var profileStackView : UIStackView!
    var profileDate : UILabel!
    var infoLabel : UILabel!
    var instituteContainerView : UIView!
    var instituteImageView : UIImageView!
    var instituteName : UILabel!
    var locationLabel : UILabel!
    var creditLabel : UILabel!
    var stacKViewLoc : UIStackView!
    var detailButton : UIButton!
    var sepearatorLineView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .white
        setUpCell()
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpCell() {
        let profileCont : UIView = {
            let view = UIView()
//            view.backgroundColor = .gray
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let profileImgView : UIView = {
            let view = UIImageView()
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let profileImg : UIImageView = {
            let view = UIImageView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.image = UIImage(named: "profile-img") ?? UIImage()
            view.setRounded()
            return view
        }()
        
        let profileDate: UILabel = {
            let view = UILabel()
            view.text = "1.12.2019"
            view.font = UIFont.systemFont(ofSize: 14)
//            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.right
            return view
        }()
        
        let profileName: UILabel = {
            let view = UILabel()
            view.text = "Dr. Digvijay Mallegowda"
            view.font = UIFont.systemFont(ofSize: 15)
            view.numberOfLines = 0
//            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let profileDesig: UILabel = {
            let view = UILabel()
            view.text = "iOS Developer"
            view.font = UIFont.systemFont(ofSize: 14)
//            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let profileStackView: UIStackView = {
            let stackView = UIStackView(arrangedSubviews: [profileName, profileDesig])
            stackView.alignment = .fill
            stackView.distribution = .fillEqually
            stackView.axis = .vertical
            stackView.translatesAutoresizingMaskIntoConstraints = false
            return stackView
        }()
        
        let infoLabel: UILabel = {
            let view = UILabel()
            view.text = "Minimally Invasive Adhesive & Esthetic Dentistry: A Review of Available Treatment Options & Materials 2019 "
            view.numberOfLines = 0
            view.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.semibold)
//            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let instituteCont : UIView = {
            let view = UIView()
//            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let instituteImg : UIImageView = {
            let view = UIImageView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.image = UIImage(named: "institute-name") ?? UIImage()
            return view
        }()
        
        let instituteName: UILabel = {
            let view = UILabel()
            view.text = "Texas A&M University College Station"
            view.font = UIFont.systemFont(ofSize: 14)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let locationName: UILabel = {
            let view = UILabel()
            view.text = "New York , NY"
            view.font = UIFont.systemFont(ofSize: 14)
//            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let creditPoint: UILabel = {
            let view = UILabel()
            view.text = "4 Credits"
            view.font = UIFont.systemFont(ofSize: 14)
//            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.right
            return view
        }()
        
        let stackView: UIStackView = {
            let stackView = UIStackView(arrangedSubviews: [locationName, creditPoint])
            stackView.alignment = .fill
            stackView.distribution = .fillEqually
            stackView.axis = .horizontal
            stackView.translatesAutoresizingMaskIntoConstraints = false
            return stackView
        }()
        
        let detailButton : UIButton = {
            let set = UIButton(type: .system)
            set.translatesAutoresizingMaskIntoConstraints = false
            set.setTitle("I am Interested", for: .normal)
            set.setTitleColor(UIColor(hexString: "39C881"), for: .normal)
            set.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            set.layer.borderWidth = 1
            set.layer.borderColor = UIColor(hexString: "39C881").cgColor
            set.layer.cornerRadius = 5
            return set
        }()
        
        let seperatorLine : UIView = {
            let view = UIView()
            view.backgroundColor = UIColor(hexString: "F6F6F6")
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        self.profileContainerView = profileCont
        self.profileImage = profileImg
        self.profileImageView = profileImgView
        self.profileStackView = profileStackView
        self.profileDate = profileDate
        
        self.infoLabel = infoLabel
        
        self.instituteContainerView = instituteCont
        self.instituteName = instituteName
        self.instituteImageView = instituteImg
        
        self.stacKViewLoc = stackView
        self.locationLabel = locationName
        self.creditLabel = creditPoint
        
        self.detailButton = detailButton
        
        self.sepearatorLineView = seperatorLine
        
        self.contentView.addSubview(profileCont)
        self.profileContainerView.addSubview(profileImgView)
        self.profileContainerView.addSubview(profileStackView)
        self.profileContainerView.addSubview(profileDate)
        profileImgView.addSubview(profileImage)
        
        self.contentView.addSubview(infoLabel)
        
        instituteCont.addSubview(instituteImg)
        instituteCont.addSubview(instituteName)
        self.contentView.addSubview(instituteCont)
        
        self.contentView.addSubview(stackView)
        
        self.contentView.addSubview(detailButton)
        
        self.contentView.addSubview(seperatorLine)
        
        NSLayoutConstraint.activate([
            
            profileCont.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16),
            profileCont.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -16),
            profileCont.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 11),
            profileCont.heightAnchor.constraint(equalToConstant: 43),
            
            profileImgView.leadingAnchor.constraint(equalTo: profileCont.leadingAnchor, constant: 0),
            profileImgView.centerYAnchor.constraint(equalTo: profileCont.centerYAnchor),
            profileImgView.heightAnchor.constraint(equalToConstant: 43),
            profileImgView.widthAnchor.constraint(equalToConstant: 43),
            
            profileImg.centerXAnchor.constraint(equalTo: profileImgView.centerXAnchor, constant: 0),
            profileImg.centerYAnchor.constraint(equalTo: profileImgView.centerYAnchor, constant: 0),
            profileImg.widthAnchor.constraint(equalTo: profileImgView.widthAnchor, multiplier: 1),
            profileImg.heightAnchor.constraint(equalTo: profileImgView.heightAnchor, multiplier: 1),
            
            
            
            profileDate.trailingAnchor.constraint(equalTo: profileCont.trailingAnchor, constant: 0),
            profileDate.topAnchor.constraint(equalTo: profileImgView.topAnchor, constant: 0),
            profileDate.widthAnchor.constraint(equalToConstant: 65),
            
            profileStackView.leadingAnchor.constraint(equalTo:profileImgView.trailingAnchor , constant : 8),
            profileStackView.trailingAnchor.constraint(equalTo:profileDate.leadingAnchor , constant : -4),
            profileStackView.topAnchor.constraint(equalTo: profileImgView.topAnchor , constant : 0),
            profileStackView.bottomAnchor.constraint(equalTo: profileImgView.bottomAnchor, constant: 0),
            
            infoLabel.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            infoLabel.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            infoLabel.topAnchor.constraint(equalTo: profileCont.bottomAnchor, constant: 20),
            
            instituteCont.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            instituteCont.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            instituteCont.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 20),
            instituteCont.heightAnchor.constraint(equalToConstant: 26),
            
            instituteImg.leadingAnchor.constraint(equalTo: instituteCont.leadingAnchor, constant: 0),
            instituteImg.centerYAnchor.constraint(equalTo: instituteCont.centerYAnchor),
            instituteImg.heightAnchor.constraint(equalToConstant: 26),
            instituteImg.widthAnchor.constraint(equalToConstant: 26),
            
            instituteName.leadingAnchor.constraint(equalTo: instituteImg.trailingAnchor, constant: 8),
            instituteName.centerYAnchor.constraint(equalTo: instituteCont.centerYAnchor),
            
            stackView.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            stackView.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            stackView.topAnchor.constraint(equalTo: instituteCont.bottomAnchor, constant: 20),
            
            
            detailButton.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor , constant : 36),
            detailButton.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -36),
            detailButton.bottomAnchor.constraint(equalTo: seperatorLine.topAnchor, constant: -10),
            detailButton.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 18),
            
            seperatorLine.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            seperatorLine.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            seperatorLine.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            seperatorLine.heightAnchor.constraint(equalToConstant: 1)
            
            ])
        
    }

}



//---------------------------------------------------------------------------------


class FeedDetailTableViewCell: UITableViewCell {
    
    var profileContainerView : UIView!
    var profileImageView : UIView!
    var profileImage : UIImageView!
    var profileName : UILabel!
    var profileDesignation : UILabel!
    var profileStackView : UIStackView!
    var profileDate : UILabel!
    var infoLabel : UILabel!
    var instituteContainerView : UIView!
    var instituteImageView : UIImageView!
    var instituteName : UILabel!
    var tutionFeeContainer : UIView!
    var tutionFeeIcon : UIImageView!
    var tutionFeeLabel : UILabel!
    
    var creditPointContainer : UIView!
    var creditPointIcon : UIImageView!
    var creditLabel : UILabel!
    
    var stacKViewLoc : UIStackView!
    var detailButton : UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .white
        setUpCell()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpCell() {
        let profileCont : UIView = {
            let view = UIView()
            //            view.backgroundColor = .gray
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let profileImgView : UIView = {
            let view = UIImageView()
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let profileImg : UIImageView = {
            let view = UIImageView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.image = UIImage(named: "profile-img") ?? UIImage()
            view.setRounded()
            return view
        }()
        
        let profileDate: UILabel = {
            let view = UILabel()
            view.text = "1.12.2019"
            view.textColor = UIColor(hexString: "39C881")
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.right
            return view
        }()
        
        let profileName: UILabel = {
            let view = UILabel()
            view.text = "Dr. Digvijay Mallegowda"
            view.font = UIFont.systemFont(ofSize: 15)
            view.numberOfLines = 0
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let profileDesig: UILabel = {
            let view = UILabel()
            view.text = "iOS Developer"
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let profileStackView: UIStackView = {
            let stackView = UIStackView(arrangedSubviews: [profileName, profileDesig])
            stackView.alignment = .fill
            stackView.distribution = .fillEqually
            stackView.axis = .vertical
            stackView.translatesAutoresizingMaskIntoConstraints = false
            return stackView
        }()
        
        let infoLabel: UILabel = {
            let view = UILabel()
            view.text = "Minimally Invasive Adhesive & Esthetic Dentistry: A Review of Available Treatment Options & Materials 2019 "
            view.numberOfLines = 0
            view.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.semibold)
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let instituteCont : UIView = {
            let view = UIView()
            //            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let instituteImg : UIImageView = {
            let view = UIImageView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.image = UIImage(named: "institute-name") ?? UIImage()
            return view
        }()
        
        let instituteName: UILabel = {
            let view = UILabel()
            view.text = "Texas A&M University College Station"
            view.font = UIFont.systemFont(ofSize: 14)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let tutionFeeLabel: UILabel = {
            let view = UILabel()
            view.text = "Tution : 1000$"
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let tutionFeeIcon: UIImageView = {
            let view = UIImageView()
            view.image = #imageLiteral(resourceName: "tution-icon")
//            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let tutionFeeContainer : UIView = {
            let view = UIView()
//            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let creditPointIcon: UIImageView = {
            let view = UIImageView()
            view.image = #imageLiteral(resourceName: "credit-point-icon")
//            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let creditPointLabel : UILabel = {
            let view = UILabel()
            view.text = "4 Credits"
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.right
            return view
        }()
        
        let creditPointContainer : UIView = {
            let view = UIView()
//            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let trainingTypeIcon: UIImageView = {
            let view = UIImageView()
            view.image = #imageLiteral(resourceName: "training-type-icon")
//            view.backgroundColor = .red
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let trainingTypeLabel : UILabel = {
            let view = UILabel()
            view.text = "Lecture, Hands on Training"
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let trainingTypeContainer : UIView = {
            let view = UIView()
            //            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let courseDescriptionHeadingIcon: UIImageView = {
            let view = UIImageView()
            view.image = #imageLiteral(resourceName: "course-description-icon")
//            view.backgroundColor = .gray
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let courseDescriptionHeadingLabel : UILabel = {
            let view = UILabel()
            view.text = "Course Description"
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let courseDescriptionHeadingContainer : UIView = {
            let view = UIView()
            //            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let courseDescriptionDetailLabel: UILabel = {
            let view = UILabel()
            view.text = "This course will assists participants to effectively diagnose and treat TMD in any dental office. The key in any treatment provided is focusing on a patient’s problem list, doing an examination and then making an accurate diagnosis."
            view.numberOfLines = 0
            view.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let checkingTimeIcon: UIImageView = {
            let view = UIImageView()
            view.image = #imageLiteral(resourceName: "clock-icon")
//            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let checkingTimeLabel : UILabel = {
            let view = UILabel()
            view.text = "Checking Time"
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let checkingTimeContainer : UIView = {
            let view = UIView()
            //            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let checkingTimeDisplayLabel: UILabel = {
            let view = UILabel()
            view.text = "10.00 AM"
            view.numberOfLines = 0
            view.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let presentationHeadingIcon: UIImageView = {
            let view = UIImageView()
            view.image = #imageLiteral(resourceName: "presentation-icon")
//            view.backgroundColor = .blue
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let presentationHeadingLabel : UILabel = {
            let view = UILabel()
            view.text = "Presentation"
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let presentationHeadingContainer : UIView = {
            let view = UIView()
            //            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let PresentationTimingsLabel: UILabel = {
            let view = UILabel()
            view.text = "10.00 AM - 5.00 PM"
            view.numberOfLines = 0
            view.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let locationHeadingIcon: UIImageView = {
            let view = UIImageView()
            view.image = #imageLiteral(resourceName: "location-icon")
//            view.backgroundColor = .black
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let locationHeadingLabel : UILabel = {
            let view = UILabel()
            view.text = "Location"
            view.font = UIFont.systemFont(ofSize: 14)
            //            view.backgroundColor = .green
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        let locationHeadingContainer : UIView = {
            let view = UIView()
            //            view.backgroundColor = UIColor.purple
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let locationAddressLabel: UILabel = {
            let view = UILabel()
            view.text = "The Denton A. Cooley, MD & Ralph C. Cooley,\nDDS University Life Center\n7440 Cambridge Street\nHouston, TX 77055 "
            view.numberOfLines = 0
            view.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
            //            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.left
            return view
        }()
        
        
        let detailButton : UIButton = {
            let set = UIButton(type: .system)
            set.translatesAutoresizingMaskIntoConstraints = false
            set.setTitle("I am Interested", for: .normal)
            set.titleLabel?.font = UIFont.systemFont(ofSize: 18)
//            set.titleLabel?.attributedText = "I am Interested".getAttributedString(fontName: nil, size: 15, colorHex: "39C881", shadowColor: nil, color: nil)
            set.backgroundColor = UIColor(hexString: "39C881")
            set.setTitleColor(.white, for: .normal)
            set.layer.borderWidth = 1
            set.layer.borderColor = UIColor.black.cgColor
            set.layer.cornerRadius = 5
            return set
        }()
        
//        let seperatorLine : UIView = {
//            let view = UIView()
//            view.backgroundColor = UIColor.lightGray
//            view.translatesAutoresizingMaskIntoConstraints = false
//            return view
//        }()
        
        self.profileContainerView = profileCont
        self.profileImage = profileImg
        self.profileImageView = profileImgView
        self.profileStackView = profileStackView
        self.profileDate = profileDate
        
        self.infoLabel = infoLabel
        
        self.instituteContainerView = instituteCont
        self.instituteName = instituteName
        self.instituteImageView = instituteImg
        
        self.tutionFeeContainer = tutionFeeContainer
        self.tutionFeeIcon = tutionFeeIcon
        self.tutionFeeLabel = tutionFeeLabel
        
        self.creditPointContainer = creditPointContainer
        self.creditPointIcon = creditPointIcon
        self.creditLabel = creditPointLabel
        
        self.detailButton = detailButton
        
        self.contentView.addSubview(profileCont)
        self.profileContainerView.addSubview(profileImgView)
        self.profileContainerView.addSubview(profileStackView)
        self.profileContainerView.addSubview(profileDate)
        profileImgView.addSubview(profileImage)
        
        self.contentView.addSubview(infoLabel)
        
        instituteCont.addSubview(instituteImg)
        instituteCont.addSubview(instituteName)
        self.contentView.addSubview(instituteCont)
        
        self.contentView.addSubview(tutionFeeContainer)
        tutionFeeContainer.addSubview(tutionFeeIcon)
        tutionFeeContainer.addSubview(tutionFeeLabel)
        
        self.contentView.addSubview(creditPointContainer)
        creditPointContainer.addSubview(creditPointIcon)
        creditPointContainer.addSubview(creditPointLabel)
        
        self.contentView.addSubview(trainingTypeContainer)
        trainingTypeContainer.addSubview(trainingTypeIcon)
        trainingTypeContainer.addSubview(trainingTypeLabel)
        
        self.contentView.addSubview(courseDescriptionHeadingContainer)
        courseDescriptionHeadingContainer.addSubview(courseDescriptionHeadingIcon)
        courseDescriptionHeadingContainer.addSubview(courseDescriptionHeadingLabel)
        
        self.contentView.addSubview(courseDescriptionDetailLabel)
        
        self.contentView.addSubview(checkingTimeContainer)
        checkingTimeContainer.addSubview(checkingTimeIcon)
        checkingTimeContainer.addSubview(checkingTimeLabel)
        
        self.contentView.addSubview(checkingTimeDisplayLabel)
        
        self.contentView.addSubview(presentationHeadingContainer)
        presentationHeadingContainer.addSubview(presentationHeadingIcon)
        presentationHeadingContainer.addSubview(presentationHeadingLabel)
        
        self.contentView.addSubview(PresentationTimingsLabel)
        
        self.contentView.addSubview(locationHeadingContainer)
        locationHeadingContainer.addSubview(locationHeadingIcon)
        locationHeadingContainer.addSubview(locationHeadingLabel)
        
        self.contentView.addSubview(locationAddressLabel)
        
        self.contentView.addSubview(detailButton)
        
        NSLayoutConstraint.activate([
            
            profileCont.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16),
            profileCont.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -16),
            profileCont.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 11),
            profileCont.heightAnchor.constraint(equalToConstant: 43),
            
            profileImgView.leadingAnchor.constraint(equalTo: profileCont.leadingAnchor, constant: 0),
            profileImgView.centerYAnchor.constraint(equalTo: profileCont.centerYAnchor),
            profileImgView.heightAnchor.constraint(equalToConstant: 43),
            profileImgView.widthAnchor.constraint(equalToConstant: 43),
            
            profileImg.centerXAnchor.constraint(equalTo: profileImgView.centerXAnchor, constant: 0),
            profileImg.centerYAnchor.constraint(equalTo: profileImgView.centerYAnchor, constant: 0),
            profileImg.widthAnchor.constraint(equalTo: profileImgView.widthAnchor, multiplier: 1),
            profileImg.heightAnchor.constraint(equalTo: profileImgView.heightAnchor, multiplier: 1),
            
            
            
            profileDate.trailingAnchor.constraint(equalTo: profileCont.trailingAnchor, constant: 0),
            profileDate.topAnchor.constraint(equalTo: profileImgView.topAnchor, constant: 0),
            profileDate.widthAnchor.constraint(equalToConstant: 65),
            
            profileStackView.leadingAnchor.constraint(equalTo:profileImgView.trailingAnchor , constant : 8),
            profileStackView.trailingAnchor.constraint(equalTo:profileDate.leadingAnchor , constant : -4),
            profileStackView.topAnchor.constraint(equalTo: profileImgView.topAnchor , constant : 0),
            profileStackView.bottomAnchor.constraint(equalTo: profileImgView.bottomAnchor, constant: 0),
            
            infoLabel.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            infoLabel.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            infoLabel.topAnchor.constraint(equalTo: profileCont.bottomAnchor, constant: 20),
            
            instituteCont.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            instituteCont.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            instituteCont.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 20),
            instituteCont.heightAnchor.constraint(equalToConstant: 26),
            
            instituteImg.leadingAnchor.constraint(equalTo: instituteCont.leadingAnchor, constant: 0),
            instituteImg.centerYAnchor.constraint(equalTo: instituteCont.centerYAnchor),
            instituteImg.heightAnchor.constraint(equalToConstant: 26),
            instituteImg.widthAnchor.constraint(equalToConstant: 26),
            
            instituteName.leadingAnchor.constraint(equalTo: instituteImg.trailingAnchor, constant: 8),
            instituteName.centerYAnchor.constraint(equalTo: instituteCont.centerYAnchor),
            
            tutionFeeContainer.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            tutionFeeContainer.topAnchor.constraint(equalTo: instituteCont.bottomAnchor, constant: 20),
            tutionFeeContainer.heightAnchor.constraint(equalToConstant: 20),
            tutionFeeContainer.widthAnchor.constraint(equalTo: profileContainerView.widthAnchor, multiplier: 0.5),
            
            tutionFeeIcon.widthAnchor.constraint(equalToConstant: 20),
            tutionFeeIcon.heightAnchor.constraint(equalToConstant: 20),
            tutionFeeIcon.centerYAnchor.constraint(equalTo: tutionFeeContainer.centerYAnchor, constant: 0),
            tutionFeeIcon.leadingAnchor.constraint(equalTo: tutionFeeContainer.leadingAnchor, constant: 0),
            
            tutionFeeLabel.leadingAnchor.constraint(equalTo: tutionFeeIcon.trailingAnchor, constant: 8),
            tutionFeeLabel.centerYAnchor.constraint(equalTo: tutionFeeContainer.centerYAnchor, constant: 0),
            tutionFeeLabel.trailingAnchor.constraint(equalTo: tutionFeeContainer.trailingAnchor, constant: 0),
            
            creditPointContainer.rightAnchor.constraint(equalTo:profileCont.rightAnchor),
            creditPointContainer.topAnchor.constraint(equalTo: tutionFeeContainer.topAnchor, constant: 0),
            creditPointContainer.bottomAnchor.constraint(equalTo: tutionFeeContainer.bottomAnchor, constant: 0),
            creditPointContainer.leadingAnchor.constraint(equalTo: tutionFeeContainer.trailingAnchor, constant: 6),
            
            creditPointLabel.centerYAnchor.constraint(equalTo: creditPointContainer.centerYAnchor, constant: 0),
            creditPointLabel.trailingAnchor.constraint(equalTo: creditPointContainer.trailingAnchor, constant: 0),
            
            creditPointIcon.widthAnchor.constraint(equalTo: tutionFeeIcon.widthAnchor, constant: 0),
            creditPointIcon.heightAnchor.constraint(equalTo: tutionFeeIcon.heightAnchor, constant: 0),
            creditPointIcon.centerYAnchor.constraint(equalTo: creditPointContainer.centerYAnchor, constant: 0),
            creditPointIcon.trailingAnchor.constraint(equalTo: creditPointLabel.leadingAnchor, constant: -8),
            
            trainingTypeContainer.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            trainingTypeContainer.topAnchor.constraint(equalTo: tutionFeeContainer.bottomAnchor, constant: 20),
            trainingTypeContainer.heightAnchor.constraint(equalTo: tutionFeeContainer.heightAnchor, constant: 0),
            trainingTypeContainer.rightAnchor.constraint(equalTo: profileCont.rightAnchor, constant: 0),
            
            trainingTypeIcon.widthAnchor.constraint(equalTo: tutionFeeIcon.widthAnchor, constant: 0),
            trainingTypeIcon.heightAnchor.constraint(equalTo: tutionFeeIcon.heightAnchor, constant: 0),
            trainingTypeIcon.centerYAnchor.constraint(equalTo: trainingTypeContainer.centerYAnchor, constant: 0),
            trainingTypeIcon.leadingAnchor.constraint(equalTo: trainingTypeContainer.leadingAnchor, constant: 0),
            
            trainingTypeLabel.leadingAnchor.constraint(equalTo: trainingTypeIcon.trailingAnchor, constant: 8),
            trainingTypeLabel.centerYAnchor.constraint(equalTo: trainingTypeContainer.centerYAnchor, constant: 0),
            trainingTypeLabel.trailingAnchor.constraint(equalTo: trainingTypeContainer.trailingAnchor, constant: 0),
            
            courseDescriptionHeadingContainer.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            courseDescriptionHeadingContainer.topAnchor.constraint(equalTo: trainingTypeContainer.bottomAnchor, constant: 20),
            courseDescriptionHeadingContainer.heightAnchor.constraint(equalTo: tutionFeeContainer.heightAnchor, constant: 0),
            courseDescriptionHeadingContainer.rightAnchor.constraint(equalTo: profileCont.rightAnchor, constant: 0),
            
            courseDescriptionHeadingIcon.widthAnchor.constraint(equalTo: tutionFeeIcon.widthAnchor, constant: 0),
            courseDescriptionHeadingIcon.heightAnchor.constraint(equalTo: tutionFeeIcon.heightAnchor, constant: 0),
            courseDescriptionHeadingIcon.centerYAnchor.constraint(equalTo: courseDescriptionHeadingContainer.centerYAnchor, constant: 0),
            courseDescriptionHeadingIcon.leadingAnchor.constraint(equalTo: courseDescriptionHeadingContainer.leadingAnchor, constant: 0),
            
            courseDescriptionHeadingLabel.leadingAnchor.constraint(equalTo: courseDescriptionHeadingIcon.trailingAnchor, constant: 8),
            courseDescriptionHeadingLabel.centerYAnchor.constraint(equalTo: courseDescriptionHeadingContainer.centerYAnchor, constant: 0),
            courseDescriptionHeadingLabel.trailingAnchor.constraint(equalTo: courseDescriptionHeadingContainer.trailingAnchor, constant: 0),
            
            courseDescriptionDetailLabel.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            courseDescriptionDetailLabel.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            courseDescriptionDetailLabel.topAnchor.constraint(equalTo: courseDescriptionHeadingContainer.bottomAnchor, constant: 20),
            
            checkingTimeContainer.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            checkingTimeContainer.topAnchor.constraint(equalTo: courseDescriptionDetailLabel.bottomAnchor, constant: 20),
            checkingTimeContainer.heightAnchor.constraint(equalTo: tutionFeeContainer.heightAnchor, constant: 0),
            checkingTimeContainer.rightAnchor.constraint(equalTo: profileCont.rightAnchor, constant: 0),
            
            checkingTimeIcon.widthAnchor.constraint(equalTo: tutionFeeIcon.widthAnchor, constant: 0),
            checkingTimeIcon.heightAnchor.constraint(equalTo: tutionFeeIcon.heightAnchor, constant: 0),
            checkingTimeIcon.centerYAnchor.constraint(equalTo: checkingTimeContainer.centerYAnchor, constant: 0),
            checkingTimeIcon.leadingAnchor.constraint(equalTo: checkingTimeContainer.leadingAnchor, constant: 0),
            
            checkingTimeLabel.leadingAnchor.constraint(equalTo: checkingTimeIcon.trailingAnchor, constant: 8),
            checkingTimeLabel.centerYAnchor.constraint(equalTo: checkingTimeContainer.centerYAnchor, constant: 0),
            checkingTimeLabel.trailingAnchor.constraint(equalTo: checkingTimeContainer.trailingAnchor, constant: 0),
            
            checkingTimeDisplayLabel.leftAnchor.constraint(equalTo:checkingTimeLabel.leftAnchor),
            checkingTimeDisplayLabel.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            checkingTimeDisplayLabel.topAnchor.constraint(equalTo: checkingTimeContainer.bottomAnchor, constant: 12),
            
            presentationHeadingContainer.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            presentationHeadingContainer.topAnchor.constraint(equalTo: checkingTimeDisplayLabel.bottomAnchor, constant: 20),
            presentationHeadingContainer.heightAnchor.constraint(equalTo: tutionFeeContainer.heightAnchor, constant: 0),
            presentationHeadingContainer.rightAnchor.constraint(equalTo: profileCont.rightAnchor, constant: 0),
            
            presentationHeadingIcon.widthAnchor.constraint(equalTo: tutionFeeIcon.widthAnchor, constant: 0),
            presentationHeadingIcon.heightAnchor.constraint(equalTo: tutionFeeIcon.heightAnchor, constant: 0),
            presentationHeadingIcon.centerYAnchor.constraint(equalTo: presentationHeadingContainer.centerYAnchor, constant: 0),
            presentationHeadingIcon.leadingAnchor.constraint(equalTo: presentationHeadingContainer.leadingAnchor, constant: 0),
            
            presentationHeadingLabel.leadingAnchor.constraint(equalTo: checkingTimeIcon.trailingAnchor, constant: 8),
            presentationHeadingLabel.centerYAnchor.constraint(equalTo: presentationHeadingContainer.centerYAnchor, constant: 0),
            presentationHeadingLabel.trailingAnchor.constraint(equalTo: presentationHeadingContainer.trailingAnchor, constant: 0),
            
            PresentationTimingsLabel.leftAnchor.constraint(equalTo:checkingTimeLabel.leftAnchor),
            PresentationTimingsLabel.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            PresentationTimingsLabel.topAnchor.constraint(equalTo: presentationHeadingContainer.bottomAnchor, constant: 12),
            
            locationHeadingContainer.leftAnchor.constraint(equalTo:profileCont.leftAnchor),
            locationHeadingContainer.topAnchor.constraint(equalTo: PresentationTimingsLabel.bottomAnchor, constant: 20),
            locationHeadingContainer.heightAnchor.constraint(equalTo: locationHeadingContainer.heightAnchor, constant: 0),
            locationHeadingContainer.rightAnchor.constraint(equalTo: profileCont.rightAnchor, constant: 0),
            
            locationHeadingIcon.widthAnchor.constraint(equalTo: tutionFeeIcon.widthAnchor, constant: 0),
            locationHeadingIcon.heightAnchor.constraint(equalTo: tutionFeeIcon.heightAnchor, constant: 0),
            locationHeadingIcon.centerYAnchor.constraint(equalTo: locationHeadingContainer.centerYAnchor, constant: 0),
            locationHeadingIcon.leadingAnchor.constraint(equalTo: locationHeadingContainer.leadingAnchor, constant: 0),
            
            locationHeadingLabel.leadingAnchor.constraint(equalTo: presentationHeadingIcon.trailingAnchor, constant: 8),
            locationHeadingLabel.centerYAnchor.constraint(equalTo: locationHeadingContainer.centerYAnchor, constant: 0),
            locationHeadingLabel.trailingAnchor.constraint(equalTo: locationHeadingContainer.trailingAnchor, constant: 0),
            
            locationAddressLabel.leftAnchor.constraint(equalTo:checkingTimeLabel.leftAnchor),
            locationAddressLabel.rightAnchor.constraint(equalTo: profileCont.rightAnchor),
            locationAddressLabel.topAnchor.constraint(equalTo: locationHeadingContainer.bottomAnchor, constant: 12),
            
            detailButton.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 0),
            detailButton.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            detailButton.topAnchor.constraint(equalTo: locationAddressLabel.bottomAnchor, constant: 32),
            detailButton.heightAnchor.constraint(equalToConstant: 38),
            detailButton.widthAnchor.constraint(equalToConstant: 273)
            
            
            ])
        
    }
    
}
