//
//
//  Created by digvijay mallegowda on 2/10/19.
//  Copyright © 2019 digvijay mallegowda. All rights reserved.
//


import Foundation

protocol TransducerState: Hashable {}

protocol TransducerTrigger: Hashable {
	associatedtype State: TransducerState
}

extension Transducer {
	typealias Guard = () -> Bool
	typealias Action = () -> Void
	
	struct Condition<State, Trigger: TransducerTrigger>: Hashable where Trigger.State == State {
		let origin: State
		let trigger: Trigger
	}
	
	struct Effect<State: TransducerState> {
		let `guard`: Guard
		let destination: State
		let action: Action
	}
}

struct ArrayedDictionary<Key: Hashable, Value> {
	private var dictionary: [Key: [Value]] = [:]
	
	subscript(key: Key) -> [Value]? {
		return dictionary[key]
	}
	
	mutating func add(value: Value, for key: Key) {
		if let existingArray = dictionary[key] {
			dictionary[key] = existingArray + [value]
		} else {
			dictionary[key] = [value]
		}
	}
}

// MARK: - Transducer
class Transducer<State, Trigger: TransducerTrigger> where Trigger.State == State {
	private var states: [State]
	private var transitions = ArrayedDictionary<Condition<State, Trigger>, Effect<State>>()
	private var entries = ArrayedDictionary<State, Action>()
	private var exits = ArrayedDictionary<State, Action>()
	private var started = false
	
	init(states: State...) {
		self.states = states
	}
	
	func add(_ condition: Condition<State, Trigger>, with effect: Effect<State>) {
		transitions.add(value: effect, for: condition)
	}
	
	func addEntry(for state: State, withAction action: @escaping Action) {
		entries.add(value: action, for: state)
	}
	
	func addExit(for state: State, withAction action: @escaping Action) {
		exits.add(value: action, for: state)
	}
	
	func start() {
		guard !started else {
			return
		}
		for state in states {
			entries[state]?.forEach { $0() }
		}
		started = true
	}

	func fire(_ trigger: Trigger) {
		assert(started, "Trying to fire the transducer before starting it")
		for (index, state) in states.enumerated() {
			guard let effects = transitions[Condition(origin: state, trigger: trigger)] else {
				continue
			}
			for effect in effects {
				guard effect.guard() else {
					continue
				}
				exits[state]?.forEach { $0() }
				states[index] = effect.destination
				effect.action()
				entries[effect.destination]?.forEach { $0() }
				break
			}
		}
	}
}

// Chainable methods
extension Transducer {
	func addTransition(withOrigin origin: State, trigger: Trigger, guard: @escaping Guard = { true }, destination: State, action: @escaping Action = {}) {
		let condition = Condition(origin: origin, trigger: trigger)
		let effect = Effect(guard: `guard`, destination: destination, action: action)
		add(condition, with: effect)
	}
	
	typealias ParametrizedGuard<Object: AnyObject> = (Object) -> Bool
	typealias ParametrizedAction<Object: AnyObject> = (Object) -> Void
	
	@discardableResult
	func addTransition<Object: AnyObject>(on object: Object, withOrigin origin: State, trigger: Trigger, guard: @escaping ParametrizedGuard<Object> = { _ in true }, destination: State, action: @escaping ParametrizedAction<Object> = { _ in } ) -> Transducer {
		let unownedGuard = { [unowned object] in `guard`(object) }
		let unownedAction = { [unowned object] in action(object) }
		addTransition(withOrigin: origin, trigger: trigger, guard: unownedGuard, destination: destination, action: unownedAction)
		return self
	}
	
	@discardableResult
	func addEntry<Object: AnyObject>(on object: Object, for state: State, withAction action: @escaping ParametrizedAction<Object>) -> Transducer {
		addEntry(for: state, withAction: { action(object) })
		return self
	}
	
	@discardableResult
	func addExit<Object: AnyObject>(on object: Object, for state: State, withAction action: @escaping ParametrizedAction<Object>) -> Transducer {
		addExit(for: state, withAction: { action(object) })
		return self
	}
}

extension Transducer {
	struct ChainingContext<Object: AnyObject> {
		let transducer: Transducer
		let object: Object
		
		@discardableResult
		func addEntry(for state: State, withAction action: @escaping ParametrizedAction<Object>) -> ChainingContext {
			transducer.addEntry(on: object, for: state, withAction: action)
			return self
		}
		
		@discardableResult
		func addExit(for state: State, withAction action: @escaping ParametrizedAction<Object>) -> ChainingContext {
			transducer.addExit(on: object, for: state, withAction: action)
			return self
		}
		
		@discardableResult
		func addTransition(withOrigin origin: State, trigger: Trigger, guard: @escaping ParametrizedGuard<Object> = { _ in true }, destination: State, action: @escaping ParametrizedAction<Object> = { _ in } ) -> ChainingContext {
			transducer.addTransition(on: object, withOrigin: origin, trigger: trigger, guard: `guard`, destination: destination, action: action)
			return self
		}
	}

	subscript<Object: AnyObject>(object: Object) -> ChainingContext<Object> {
		return ChainingContext(transducer: self, object: object)
	}
}

extension Transducer {
	struct Rule<Object: AnyObject> {
		let origin: State
		let trigger: Trigger
		let `guard`: ParametrizedGuard<Object>
		let destination: State
		let action: ParametrizedAction<Object>
		
		init(origin: State, trigger: Trigger, `guard`: @escaping ParametrizedGuard<Object> = { _ in true }, destination: State, action: @escaping Transducer<State, Trigger>.ParametrizedAction<Object> = { _ in }) {
			self.origin = origin
			self.trigger = trigger
			self.guard = `guard`
			self.destination = destination
			self.action = action
		}
	}
}

infix operator -->: BitwiseShiftPrecedence

func += <O, S, T> (lhs: Transducer<S, T>.ChainingContext<O>, rhs: Transducer<S, T>.Rule<O>) {
	lhs.addTransition(withOrigin: rhs.origin, trigger: rhs.trigger, guard: rhs.guard, destination: rhs.destination, action: rhs.action)
}

func --> <O, S, T>(lhs: (S, T), rhs: S) -> Transducer<S, T>.Rule<O> {
	let (origin, trigger) = lhs
	return Transducer<S, T>.Rule<O>(origin: origin, trigger: trigger, destination: rhs)
}

func --> <O, S, T>(lhs: (S, T, Transducer<S, T>.ParametrizedGuard<O>), rhs: S) -> Transducer<S, T>.Rule<O> {
	let (origin, trigger, `guard`) = lhs
	return Transducer<S, T>.Rule<O>(origin: origin, trigger: trigger, guard: `guard`, destination: rhs)
}

func --> <O, S, T>(lhs: (S, T), rhs: (S, Transducer<S, T>.ParametrizedAction<O>)) -> Transducer<S, T>.Rule<O> {
	let (origin, trigger) = lhs
	let (destination, action) = rhs
	return Transducer<S, T>.Rule<O>(origin: origin, trigger: trigger, destination: destination, action: action)
}

func --> <O, S, T>(lhs: (S, T, Transducer<S, T>.ParametrizedGuard<O>), rhs: (S, Transducer<S, T>.ParametrizedAction<O>)) -> Transducer<S, T>.Rule<O> {
	let (origin, trigger, `guard`) = lhs
	let (destination, action) = rhs
	return Transducer<S, T>.Rule<O>(origin: origin, trigger: trigger, guard: `guard`, destination: destination, action: action)
}

extension Transducer {
	enum StateTrigger {
		case entry
		case exit
	}
	
	struct StateRule<Object: AnyObject> {
		let state: State
		let stateTrigger: StateTrigger
		let action: ParametrizedAction<Object>
	}
}

func += <O, S, T>(lhs: Transducer<S, T>.ChainingContext<O>, rhs: Transducer<S, T>.StateRule<O>) {
	switch rhs.stateTrigger {
	case .entry: lhs.addEntry(for: rhs.state, withAction: rhs.action)
	case .exit: lhs.addExit(for: rhs.state, withAction: rhs.action)
	}
}

func --> <O, S, T>(lhs: (S, Transducer<S, T>.StateTrigger), rhs: @escaping Transducer<S, T>.ParametrizedAction<O>) -> Transducer<S, T>.StateRule<O> {
	let (state, stateTrigger) = lhs
	return Transducer<S, T>.StateRule(state: state, stateTrigger: stateTrigger, action: rhs)
}
