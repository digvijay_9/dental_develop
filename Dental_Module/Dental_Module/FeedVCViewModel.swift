//
//  FeedVCViewModel.swift
//  Dental_Module
//
//  Created by digvijay mallegowda on 2/13/19.
//  Copyright © 2019 digvijay mallegowda. All rights reserved.
//

import Foundation
import  UIKit
import CoreLocation


class FeedVCViewModel : NSObject {
    
    private let locationManager = CLLocationManager()
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
    }
    
}

// Mark : - Core Location Delegate
extension FeedVCViewModel : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
            
        case .notDetermined:
            print("not detrmined")
        case .restricted:
            print("restrict")
        case .denied:
            print("denied")
        case .authorizedAlways:
            print("authorized always")
        case .authorizedWhenInUse:
            print("when in use")
        }
    }
}
