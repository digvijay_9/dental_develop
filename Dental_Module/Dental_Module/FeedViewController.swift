//
//  ViewController.swift
//  Dental_Module
//
//  Created by digvijay mallegowda on 2/8/19.
//  Copyright © 2019 digvijay mallegowda. All rights reserved.
//

import UIKit

let FeedCell = "FeedCell"
let FeedDetailCell = "FeedDetailCell"

class DummyVC : UIViewController {
    var button : UIButton!
    override func viewDidLoad() {
        self.view.backgroundColor = .white
        button = UIButton(frame: CGRect(x: 140, y: 250, width: 100, height: 50))
        button.setTitle("NEXT", for: .normal)
//        button.center =  self.view.center
        button.backgroundColor = .gray
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(goNext(_:)), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    
    @objc func goNext( _ sender : UIButton) {
        self.navigationController?.pushViewController(FeedViewController(), animated: true)
    }
}

class FeedViewController: UIViewController , UISearchBarDelegate, UISearchResultsUpdating , UITableViewDataSource ,UIActionSheetDelegate ,  UITableViewDelegate {
    
    var search : UISearchController!
    var resultVC : ResultTableViewController!
    var tableView : UITableView!
    var searchNearContainerView : UIView!
    var searchNearImageView : UIImageView!
    var searchNearLabel : UILabel!
    var activityIndicator : UIActivityIndicatorView!
    
//    let viewModel = FeedVCViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationItem.backBarButtonItem?.title = ""
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setUpView()
        setUpSearch()
        tableView.register(FeedTableViewCell.self , forCellReuseIdentifier: FeedCell)
//        tableView.isHidden = true
        setUpLoadingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func setUpSearch() {
        resultVC = ResultTableViewController()
        search = UISearchController(searchResultsController: resultVC)
        search.searchResultsUpdater = self
        search.searchBar.delegate = self
        search.searchBar.sizeToFit()
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Search Course by"
        search.searchBar.scopeButtonTitles = ["Area of Intrest","Course Provider","City"]
        let scopeButtonAttributes : [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor : UIColor.black , NSAttributedString.Key.font: UIFont(name: "HK Grotesk" , size: 14) ?? UIFont.boldSystemFont(ofSize: 14)]
        search.searchBar.setScopeBarButtonTitleTextAttributes(scopeButtonAttributes, for: .normal)
//        UISegmentedControl.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setti
        UISegmentedControl.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .clear
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.systemFont(ofSize: 14)
        self.navigationItem.searchController = search
        definesPresentationContext = true
    }
    
    func setUpView() {
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.backgroundColor = .white
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font:  UIFont.systemFont(ofSize: 24)]
        self.navigationItem.title = "CE Courses"
        
        let tableView : UITableView = {
            let set = UITableView()
            set.translatesAutoresizingMaskIntoConstraints = false
            set.backgroundColor = .red
            set.separatorStyle = .none
            set.dataSource = self
            set.delegate = self
            set.rowHeight = UITableView.automaticDimension
            set.estimatedRowHeight = 300
            return set
        }()
        
        let searchNearCont : UIView = {
            let view = UIView()
                        view.backgroundColor = .gray
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let searchNearImg : UIImageView = {
            let view = UIImageView()
            view.backgroundColor = .red
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        let searchNearLabel: UILabel = {
            let view = UILabel()
            view.text = "Search near me"
            view.font = UIFont.systemFont(ofSize: 14)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.textAlignment = NSTextAlignment.center
            return view
        }()
        
        self.tableView = tableView
        self.searchNearImageView = searchNearImg
        self.searchNearLabel = searchNearLabel
        self.searchNearContainerView = searchNearCont
        
        self.view.addSubview(searchNearCont)
        searchNearCont.addSubview(searchNearImg)
        searchNearCont.addSubview(searchNearLabel)
        
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            
            searchNearCont.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            searchNearCont.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            searchNearCont.heightAnchor.constraint(equalToConstant: 50),
            searchNearCont.widthAnchor.constraint(equalToConstant: 117),
            
            searchNearLabel.leadingAnchor.constraint(equalTo: searchNearCont.leadingAnchor),
            searchNearLabel.trailingAnchor.constraint(equalTo: searchNearCont.trailingAnchor),
            searchNearLabel.bottomAnchor.constraint(equalTo: searchNearCont.bottomAnchor),
            
            searchNearImg.centerXAnchor.constraint(equalTo: searchNearCont.centerXAnchor),
            searchNearImg.heightAnchor.constraint(equalToConstant: 28),
            searchNearImg.widthAnchor.constraint(equalToConstant: 28),
            searchNearImg.bottomAnchor.constraint(equalTo: searchNearLabel.topAnchor, constant: -2),
            
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)])
        
    }
    
    func setUpLoadingView() {
        let activity : UIActivityIndicatorView = {
            let view = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        self.activityIndicator = activity
        activity.startAnimating()
        self.searchNearImageView.addSubview(activity)
        NSLayoutConstraint.activate([
            activity.centerXAnchor.constraint(equalTo: self.searchNearImageView.centerXAnchor),
            activity.centerYAnchor.constraint(equalTo: self.searchNearImageView.centerYAnchor)
            ])
        
    }
    

}


//Mark : Search Updating and filter
extension FeedViewController {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("clicked")
        scopeBottonClicked()
    }
    
    func scopeBottonClicked() {
        let alert = UIAlertController(title: "Title", message: "Please Select an Option", preferredStyle: .actionSheet)
        
//        alert.addAction(UIAlertAction(title: "Approve", style: .default , handler:{ (UIAlertAction)in
//            print("User click Approve button")
//        }))
        let filter1 = UIAlertAction(title: "Edit", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
        })
        let filterImage = UIImage(named: "filter-globe") ?? UIImage()

        filter1.setValue(filterImage, forKey: "image")
        
        alert.addAction(filter1)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
}



//Mark : TableView Data Source and Delegates
extension FeedViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell, for: indexPath) as! FeedTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = FeedDetailViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}







class ResultTableViewController: UITableViewController {
    
    // MARK: - Properties
    
    var filteredProducts = [Product]()

    
    // MARK: - Constants
    
    static let tableViewCellIdentifier = "cellID"
    private static let nibName = "TableCell"
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ResultTableViewController.tableViewCellIdentifier)
        filteredProducts = [Product(title: "one"),Product(title: "oneTwo"),Product(title: "oneTwoThree")]
    }
    
    // MARK: - Configuration
    
    func configureCell(_ cell: UITableViewCell, forProduct product: Product) {
        cell.textLabel?.text = "check"
        
        cell.detailTextLabel?.text = ""
    }
    
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Courses from"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredProducts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ResultTableViewController.tableViewCellIdentifier, for: indexPath)
        let product = filteredProducts[indexPath.row]
        configureCell(cell, forProduct: product)
        
        return cell
    }
}

struct Product {
    var title: String
}



class FeedDetailViewController : UIViewController , UITableViewDataSource {
    
    var staticTableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        staticTableView.register(FeedDetailTableViewCell.self , forCellReuseIdentifier: FeedDetailCell)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    func setUpView() {
        self.view.backgroundColor = .white
       
        
        let tableView : UITableView = {
            let set = UITableView()
            set.translatesAutoresizingMaskIntoConstraints = false
            set.backgroundColor = .white
            set.separatorStyle = .none
            set.dataSource = self
            set.rowHeight = UITableView.automaticDimension
            set.estimatedRowHeight = 800
            return set
        }()
        
        
        self.staticTableView = tableView
        self.view.addSubview(tableView)
        
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
            ])
        
    }
}

extension FeedDetailViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedDetailCell, for: indexPath) as! FeedDetailTableViewCell
        
        return cell
    }
}
