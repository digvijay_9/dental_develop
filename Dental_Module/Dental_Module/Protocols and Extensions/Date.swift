//
//
//  Created by digvijay mallegowda on 2/10/19.
//  Copyright © 2019 digvijay mallegowda. All rights reserved.
//


import Foundation

extension Date {
	var datedText: String {
		let formatter = DateFormatter()
		formatter.dateStyle = .long
		formatter.timeStyle = .none
		let formatted = formatter.string(from: self)
		return "Updated on " + formatted
	}
}
