//
//  Extensions.swift
//  Dental_Module
//
//  Created by digvijay mallegowda on 2/13/19.
//  Copyright © 2019 digvijay mallegowda. All rights reserved.
//

import Foundation
import UIKit


//extension  UIImage {
//    var cirlceMask : UIImage? {
//        let square = CGSize(width: min(size.width, size.height), height: min(size.width, size.height))
//        let frameImage = CGRect(origin: CGPoint(x: 0, y: 0), size: square)
//        let imageView = UIImageView(frame: frameImage)
//        imageView.contentMode = .scaleAspectFill
//        imageView.image = self
//        let cornerRadius = square.height / 2
////        imageView.layer.masksToBounds = true
////        UIGraphicsBeginImageContext(imageView.bounds.size)
////        if let context = UIGraphicsGetCurrentContext() {
////         imageView.layer.render(in: context)
////        }
////        let result = UIGraphicsGetImageFromCurrentImageContext()
////        UIGraphicsEndImageContext()
//        UIGraphicsBeginImageContextWithOptions(square, false, 1.0)
//        UIBezierPath(roundedRect: frameImage, cornerRadius: cornerRadius).addClip()
//        self.draw(in: frameImage)
//        let result = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return result
//    }
    
//    var roundedImage : UIImage? {
//        let square = CGSize(width: min(size.width, size.height), height: min(size.width, size.height))
//        let frameImage = CGRect(origin: CGPoint(x: 0, y: 0), size: square)
//        let imageView = UIImageView(frame: frameImage)
//        imageView.contentMode = .scaleAspectFill
//        imageView.image = self
//        
//        
//        let cornerRadius = square.height / 2
//        let imageView = UIImageView(image: self)
//        let layer = imageView.layer
//        layer.masksToBounds = false
//        layer.cornerRadius = imageView.frame.height / 2
//        imageView.clipsToBounds = true
//        imageView.layoutIfNeeded()
//        return imageView.image
//    }
    
//}


extension UIImageView {
    func setRounded() {
        let radius = self.frame.size.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
}


extension String{
    func getAttributedString(fontName : String?, size : Int , colorHex : String? , shadowColor : String? , color : UIColor?) -> NSAttributedString {
        var attributesToApply: [NSAttributedString.Key: Any] = [:]
        if let fontName = fontName  {
            attributesToApply[NSAttributedString.Key.font] =  ( UIFont(name: fontName, size: CGFloat(size) ) ?? UIFont.systemFont(ofSize: CGFloat(size)) )
        } else {
            attributesToApply[NSAttributedString.Key.font]  = UIFont.systemFont(ofSize: CGFloat(size))
        }
        
        if let colorHex = colorHex {
            attributesToApply[NSAttributedString.Key.foregroundColor] = UIColor(hexString: colorHex)
        }
        
        if let color = color {
            attributesToApply[NSAttributedString.Key.foregroundColor] = color
        }
        
        if let shadowColor = shadowColor {
            let shadow = NSShadow()
            shadow.shadowColor = UIColor(hexString: shadowColor)
            shadow.shadowBlurRadius = 5
            attributesToApply[.shadow] = shadow
        }
        
        return NSAttributedString(string: self , attributes: attributesToApply)
        
    }
    
    
    
}



extension UIColor{
    convenience init (hexString:String) {
        var cleanString = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cleanString.hasPrefix("#")) {
            cleanString.remove(at: cleanString.startIndex)
        }
        var rgbValue = UInt32()
        Scanner(string: cleanString).scanHexInt32(&rgbValue)
        let a,r,g,b:UInt32
        switch cleanString.count{
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (rgbValue >> 8) * 17, (rgbValue >> 4 & 0xF) * 17, (rgbValue & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, rgbValue >> 16, rgbValue >> 8 & 0xFF, rgbValue & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (rgbValue >> 24, rgbValue >> 16 & 0xFF, rgbValue >> 8 & 0xFF, rgbValue & 0xFF)
        default:
            (a, r, g, b) = (0, 0, 0, 255)
        }
        self.init(
            red:    CGFloat(r)/255,
            green:  CGFloat(g)/255,
            blue:   CGFloat(b)/255,
            alpha:  CGFloat(a)/255)
    }
}


extension Date {
    func formatter(format : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension UIImageView {
    
    func setCustomImage(_ imgURLString: String?) {
        guard let imageURLString = imgURLString else {
            
            return
        }
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: URL(string: imageURLString)!)
            DispatchQueue.main.async {
                self.image = data != nil ? UIImage(data: data!) : UIImage()
            }
        }
    }
}

protocol CEReusableView: class {}

extension CEReusableView where Self: UITableViewCell {
    
    static var reuseIdentifier: String {
        return String(describing: type(of: self))
    }
}

extension UITableView {
    
    // To register UITableViewCell class
    func registerCellFromClass<T: UITableViewCell>(_: T.Type) where T: CEReusableView {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
}


//This method takes a base URL that specifies the actual directory on the file system where we want to store a file
extension URL {
    func fileUrl(withBaseURL baseURL: URL) -> URL? {
        assert(!isFileURL, "A file URL can be created only for a web URL, to avoid double indirection in the encoding")
        guard let percentEscaped = absoluteString.addingPercentEncoding(withAllowedCharacters: .alphanumerics) else {
            return nil
        }
        return baseURL
            .appendingPathComponent(percentEscaped)
            .appendingPathExtension("json")
    }
}



extension Date {
    var dateText: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        let formatted = formatter.string(from: self)
        return "Updated on " + formatted
    }
}

