//
//
//  Created by digvijay mallegowda on 2/10/19.
//  Copyright © 2019 digvijay mallegowda. All rights reserved.
//

import UIKit

extension UIColor {
	static let mediumCarmine = UIColor(named: "Medium Carmine")!
	static let cork = UIColor(named: "Cork")!
	static let dustyGray = UIColor(named: "Dusty Grey")!
	static let alto = UIColor(named: "Alto")!
}
