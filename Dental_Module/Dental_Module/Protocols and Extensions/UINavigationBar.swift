//
//
//  Created by digvijay mallegowda on 2/10/19.
//  Copyright © 2019 digvijay mallegowda. All rights reserved.
//


import UIKit

extension UINavigationBar {
	static func setCustomAppearance() {
		UINavigationBar.appearance().tintColor = .white
		UINavigationBar.appearance().barTintColor = .mediumCarmine
		UINavigationBar.appearance().isTranslucent = false
		
	}
}
